#
# The FROM keyword defines the base Docker image of our container.
# Get a nginx Docker image from Docker Hub tagged with 1.17.1-alpine (it’s like a version number)
FROM nginx

# Copy-paste the compiled application to the container.
COPY /dist/Angular-App-pm /usr/share/nginx/html

# The VOLUME instruction creates a mount point with the specified name and marks it
# as holding externally mounted volumes from the native host or other containers.
VOLUME /tmp
