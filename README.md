# Masteing Markdown (MD)  a way to style text on the web
https://guides.github.com/features/mastering-markdown/

# AngularAppPm

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Course References
* Courses https://github.com/joeeames
* https://johnpapa.net/
* http://www.typescriptlang.org/play/ (Typescript playground)
* https://github.com/DeborahK/Angular-GettingStarted (APM Start, APM Final)
* https://expressjs.com/ For example setup simple NodeJS server application (Angular HTTP Communication course)
  storage in files

# General References
* https://www.madewithangular.com/
* https://javascript-conference.com/blog/build-test-deployment-angular-gitlab-ci/
* https://medium.com/@wkrzywiec/build-and-run-angular-application-in-a-docker-container-b65dbbc50be8

# Dockerizing
* https://blog.codecentric.de/en/2019/03/docker-angular-dockerize-app-easily/ (ENTRYPOINT)
* https://mherman.org/blog/dockerizing-an-angular-app/

** Architectuur

App = M1+C1+C2+Cn
Module (M1) = MC1+MC2+MCn
Component (Cn) = Template + Class (Typescript) + Metadata (Extra data/attributes angular)
IDX - selector -> APP - Welcome
                  -> Product              -> Star component (nested)
                  -> Product Detail       -> Star component (nested)
                  -> Product Data Service

# Setting up environment
* Node Package Manager version >= 6.2.0 https://nodejs.org/en/download/
  Node Package Manager: install libraries, packages and applications
  Version >= 6.2.0 minimal requirement for Angular
* Code editor https://code.visualstudio.com/
* First check out Git 
  no such file or directory, scandir 'F:\homeGIT\workspace\Angular-App-pm\node_modules'
  Solution: npm install (based on package.json)

# Generating new application
* ng new <application name>
  --skip-install (do not install all dependencies, node_modules: use npm install (alias i))
  -- routing

# Extensions
* Angular Essentials Extension
* Angular v6 snippets (jpapam.me/angularessentials, for example diffent icons in Visual Studio Code)

# guards
  ng g guard auth to protect routes
 

# Bootstrapping
index.html (SPA- Single Page Application)
'<app-root></app-root>' points to component directive which is inserts template (inline with `` or seperate html)
directive is found in module bootstrapped component




# Layout
* twitter bootstrap http://getbootstrap.com
* fontawesome https://fontawesome (star rating)
* npm install bootstrap font-awesome
  * import in style.css (see course Building a template)

# Decorator
* Decorator prefixed with @ and properties (selector - directive name,   template)

# Template
* Inline "" single line or `` multi line (back ticks, ES 2015)
* Outline linked temp;late '<html file>'
  Better option for example syntax checking

# Binding
* interpolation one way binding property to template
* {{}}  <h1>{{pageTitle}}</h1>
* assignment to bound value <h1 innerText={{pageTitle}}></h1>
* calculation {{2*20+1}}
* method calling {{'Title: ' + (getTitle()}}
* concatenation {{'Title: ' + pageTitle}}

# Directives
Buildin or custom
## Structaral directive
* ngIf *ngIf='dataSource && dataSource.data.length' table tag
* ngFor *ngFor='let product of products' row tag {{product.<attribuut>}}
  different with material-table
* let of products  vs let in products
  ES2015 for of vs for in loop
  for .. of Iterates over itrable object
  for .. in iterates over the properties of an object

  let nicknames=['di', 'boo', 'punkeye']

  let nicknames=['di', 'boo', 'punkeye']
  for (let nickname of nicknames) {
    console.log(nickname);
  }
  Result: di, boo, punkeye

  for (let nickname in nicknames) {
    console.log(nickname);
  }
  Result: 0, 1, 2 (index of array)

# Pipes
## Builtin
ldate, number, decimal, percent, lowercase, uppercase, currency json, slice etc.
## Custom
For example transformCharacter

# Blueprints

# Tooling
## Angular Material 
ng add @angular/material

ng g @angular/material:material-nav --name nav see cli course John Papa
ng g @angular/material:material-dashboard --name dashboard see cli course John Papa
ng g @angular/material:material-table --name group/group-list -d -- flat see cli course John Papa custoer-list
html: pageSizeOptions

https://material.angular.io/



# Examples


# Forms
* Status of the form (inspect form page)
** ng-untouched
** ng-pristine
** ng-valid

* Styling
** documentation https://getbootstrap.com
** for example component -> forms https://getbootstrap.com/docs/4.2/components/forms/
** installation npm install --save bootstrap
** add to angular.json section styles reference to node_modules/bootstrap/dist/css/bootstrap.min.css
   or import in styles.css @import "~bootstrap/dist/css/bootstrap.min.css";

# Data binding in Agular Forms
** NgForm
   Added by Angular internal based on form tag
   #form="ngForm" acces via template reference variable #form
   Documentation go to https://angular.io and search ngform https://angular.io/api/forms/NgForm
  Get instance of ngforms with #form: {{ form | json }} form piped through json filter to get values and properties
** ngModel
   Needs name beside id !
** Two-way databinding trhough banana in a box
   [(ngModel)]="userSettings.emailOffers
   if form value changes userSettings changes


# Validation
** HTML5 Validation native attributes: required, pattern (regular expression), minlength, maxlength, min, max
*** ngNativeValidate will allow browser to validate every control (browser specific)
     
** CSS Classes for validation
*** ng-untouched, ng-touched (control did or did not got focus)
*** ng-pristine  (un-modified) ng-dirty (modified)
*** ng-valid ng-invalid

** ngModel properties for validation
*** ng-untouched, ng-touched (control did or did not got focus)
**** properties: untouched, touched
*** ng-pristine  (un-modified) ng-dirty (modified)
**** properties: pristine, dirty
*** ng-valid ng-invalid
**** properties: valid, invalid

*** field validation  (attribute ngvalidate )
**** for example hidden class="alert alert-danger" and/or css styling

*** form validation (attribute event (ngSubmit)
**** (ngSubmit)="onSubmit(form)" event with function call onSubmit in ts component
**** form comes form template reference variabele #form="ngForm"
**** css field-error with condition

*** Handling form control events HTML5 events
**** blur event field looses focus valid entire form or field or 
     add any code you like: modify form, show new region
***** (blur) "onBlur(propertyTest)"   propertyTest is template reference variable OnBlur in ts component
**** many others like focus

# Posting Forms using Observables
* Alterative for Observable, Promises


# [Injectable, Observable] Dataservice [HTTPClient]
* HTTPClientModule import in module 
* HttpClient Import from '@angular/common/http' in dataservice
* inject into constructor of dataservice

* Fake responses with https://putsreq.com
** Paste url created 'Create a putsreq' resulting in for example https://putsreq.com/w499QdyAJI2kWbFSWrAG
** edit response builder
   // Build a response
   var parsedBody = JSON.parse(request.body);
   parsedBody.id = '1234';
   parsedBody.result = 'OK';
   response.body = parsedBody;
** Hit Update button
** Run application and send 
** Returns:
   {name: "Baz", emailOffers: true, intefaceStyle: "medium", subscriptionType: "Annual", notes: "here are some notes...", …}
   emailOffers: true
   id: "1234"
   intefaceStyle: "medium"
   name: "Baz"
   notes: "here are some notes..."
   result: "OK"
   subscriptionType: "Annual"

# Handling POST errors
* generate error to client through seting response status
  // var parsedBody = JSON.parse(request.body);
  // parsedBody.id = '1234';
  // parsedBody.result = 'OK';
  response.status = 400
  response.body = { errorMessage: 'Some error has happend. Enjoy your day' };
  // response.body = parsedBody;

* replace console.log with method onHttpError(error) and add code to set two variables
* usage variables in html
    <div [hidden]="!postError"
      class="alert alert-danger">
      {{ postErrorMessage }}
    </div>
* check in onSubmit if form is valid or else show error

# Retrieving Data
* Property in class and assign to type
* Example retrieving Subscription Type with *ngFor
    <option *ngFor="let type of subscriptionTypes | async">
      {{ type }}
    </option>

    pipe async will wait until Observable has got the data 

* first test with fixed array of types
* second change variable to Observable of type string array
* third assign to variable with dataservice getSubscriptionTypes
* fourth use get instead of pot

# Third party controls vs HTML5
* standard experience over different browsers
* more functionality
* get 3Th party controls: angular.io - resources - ui components
** angular material (seperate course)
** ng-bootstrap
** prime faces
** Under community - Catalog of Angular Components and libraries
** ...
** ngx-bootstrap
*** https://valor-software.com/ngx-bootstrap/#/documentation#getting-started
*** installing
*** buttons
*** date and date ranges
    Add ./node_modules/ngx-bootstrap/datepicker/bs-datepicker.css
    to styles.css restart with ng serve-o
*** time control
*** rating control
