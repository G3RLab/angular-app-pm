import { TestBed, async, inject } from '@angular/core/testing';

import { GroupDetailGuardGuard } from './group-detail-guard.guard';

describe('GroupDetailGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GroupDetailGuardGuard]
    });
  });

  it('should ...', inject([GroupDetailGuardGuard], (guard: GroupDetailGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
