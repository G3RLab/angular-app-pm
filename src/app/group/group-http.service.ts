import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { GroupListItem } from '../models/group-list-item';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GroupHttpService {

  // private groupUrl = 'www.mywebservice.com/api/groups';
  // Local file
  // Assets in angular.json add /src/api
  private groupUrl = 'api/groups/groups.json';

  constructor(private http: HttpClient) { }

  getGroups(): Observable<GroupListItem[]> {
    // return this.http.get<GroupListItem[]>(this.groupUrl);
    /* Http client receives json:
       With generics parameter set autoatic map response object to defined type

       Methods returns a observable
       of array http calls are async
       Single async, contains only a object
    */
    return this.http.get<GroupListItem[]>(this.groupUrl).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  private handleError(err: HttpErrorResponse) {
            // in a real world app, we may send the server to some remote logging infrastructure
            // instead of just logging it to the console
            let errorMessage = '';
            if (err.error instanceof ErrorEvent) {
              // A client-side or network error occurred. Handle it accordingly.
              errorMessage = `An error occurred: ${err.error.message}`;
            } else {
              // The backend returned an unsuccessful response code.
              // The response body may contain clues as to what went wrong,
              errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
            }
            console.error(errorMessage);
            return throwError(errorMessage);
    }

}
