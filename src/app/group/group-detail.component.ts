import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupListItem } from './group-list-datasource';
import { Router } from '@angular/router';

@Component({
  /* Not needed, used displayed base on routing
     selector: 'app-group-detail',
  */
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']
})
export class GroupDetailComponent implements OnInit {
  pageTitle = 'Group Detail';
  groupListItem: GroupListItem;

  constructor(private route: ActivatedRoute, private router: Router) {
    // snapshot or observable if you have next button
    console.log(this.route.snapshot.paramMap.get('id'));
  }

  ngOnInit() {
    // convert string to numeric
    let id = +this.route.snapshot.paramMap.get('id');
    this.pageTitle += `:  ${id}`;
    this.groupListItem = {id: 1, name: 'Hydrogen', rating: 2.2, imageUrl: '/favicon.ico'};
  }

  onBack(): void {
    this.router.navigate(['/groups']);
  }

}
