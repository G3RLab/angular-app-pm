import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { fakeAsync } from '@angular/core/testing';

// Guard is a service
@Injectable({
  providedIn: 'root'
})
export class GroupDetailGuardGuard implements CanActivate {

  // Guard to prevent ids not a number or less than 1

  // Inject router
  constructor( private router: Router) {}

   // Read parameter from route
   // ActivatedRouteSnapshot: information about current route (at this moment)
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // Pull path from second element (0, 1, 2, ...)
    let id = +next.url[1].path;
    if (isNaN(id) || id < 1) {
      // real world to error page
      alert('Invalid group Id');
      this.router.navigate(['groups']);
      // stop further operation
      return false;
    }

    // activate route
    return true;
  }

}
