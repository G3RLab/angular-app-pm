import { Injectable } from '@angular/core';
import { GroupListItem } from './group-list-datasource';

/* root injector / component injector
   last is only available to component and child (nested)
   Isolates a service
   provides multiple instances of a service
   must then be registered within component

   providers: [GroupService] (property of decorator)
*/

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor() { }

  getGroups(): GroupListItem[] {
    return [
      {id: 1, name: 'Hydrogen', rating: 2.2, imageUrl: '/favicon.ico'},
      {id: 2, name: 'Helium', rating: 3.2, imageUrl: '/favicon.ico'},
      {id: 3, name: 'Lithium', rating: 4.2, imageUrl: '/favicon.ico'},
      {id: 4, name: 'Beryllium', rating: 5.0, imageUrl: '/favicon.ico'},
      {id: 5, name: 'Boron', rating: 1.2, imageUrl: '/favicon.ico'},
      {id: 6, name: 'Carbon', rating: 4.4, imageUrl: '/favicon.ico'},
      {id: 7, name: 'Nitrogen', rating: 3.4, imageUrl: '/favicon.ico'},
      {id: 8, name: 'Oxygen', rating: 2.5, imageUrl: '/favicon.ico'},
      {id: 9, name: 'Fluorine', rating: 2.6, imageUrl: '/favicon.ico'},
      {id: 10, name: 'Neon', rating: 3.0, imageUrl: '/favicon.ico'},
      {id: 11, name: 'Sodium', rating: 4.2, imageUrl: '/favicon.ico'},
      {id: 12, name: 'Magnesium', rating: 3.3, imageUrl: '/favicon.ico'},
      {id: 13, name: 'Aluminum', rating: 3.4, imageUrl: '/favicon.ico'},
      {id: 14, name: 'Silicon', rating: 3.6, imageUrl: '/favicon.ico'},
      {id: 15, name: 'Phosphorus', rating: 4.2, imageUrl: '/favicon.ico'},
      {id: 16, name: 'Sulfur', rating: 4.5, imageUrl: '/favicon.ico'},
      {id: 17, name: 'Chlorine', rating: 4.7, imageUrl: '/favicon.ico'},
      {id: 18, name: 'Argon', rating: 5.0, imageUrl: '/favicon.ico'},
      {id: 19, name: 'Potassium', rating: 3.2, imageUrl: '/favicon.ico'},
      {id: 20, name: 'Calcium', rating: 4.2, imageUrl: '/favicon.ico'},
    ];
  }
}
