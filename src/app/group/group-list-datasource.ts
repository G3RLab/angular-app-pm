import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface GroupListItem {
  id: number;
  name: string;
  rating: number;
  imageUrl: string;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: GroupListItem[] = [
  {id: 1, name: 'Hydrogen', rating: 2.2, imageUrl: '/favicon.ico'},
  {id: 2, name: 'Helium', rating: 3.2, imageUrl: '/favicon.ico'},
  {id: 3, name: 'Lithium', rating: 4.2, imageUrl: '/favicon.ico'},
  {id: 4, name: 'Beryllium', rating: 5.0, imageUrl: '/favicon.ico'},
  {id: 5, name: 'Boron', rating: 1.2, imageUrl: '/favicon.ico'},
  {id: 6, name: 'Carbon', rating: 4.4, imageUrl: '/favicon.ico'},
  {id: 7, name: 'Nitrogen', rating: 3.4, imageUrl: '/favicon.ico'},
  {id: 8, name: 'Oxygen', rating: 2.5, imageUrl: '/favicon.ico'},
  {id: 9, name: 'Fluorine', rating: 2.6, imageUrl: '/favicon.ico'},
  {id: 10, name: 'Neon', rating: 3.0, imageUrl: '/favicon.ico'},
  {id: 11, name: 'Sodium', rating: 4.2, imageUrl: '/favicon.ico'},
  {id: 12, name: 'Magnesium', rating: 3.3, imageUrl: '/favicon.ico'},
  {id: 13, name: 'Aluminum', rating: 3.4, imageUrl: '/favicon.ico'},
  {id: 14, name: 'Silicon', rating: 3.6, imageUrl: '/favicon.ico'},
  {id: 15, name: 'Phosphorus', rating: 4.2, imageUrl: '/favicon.ico'},
  {id: 16, name: 'Sulfur', rating: 4.5, imageUrl: '/favicon.ico'},
  {id: 17, name: 'Chlorine', rating: 4.7, imageUrl: '/favicon.ico'},
  {id: 18, name: 'Argon', rating: 5.0, imageUrl: '/favicon.ico'},
  {id: 19, name: 'Potassium', rating: 3.2, imageUrl: '/favicon.ico'},
  {id: 20, name: 'Calcium', rating: 4.2, imageUrl: '/favicon.ico'},
];

/**
 * Data source for the GroupList view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class GroupListDataSource extends DataSource<GroupListItem> {
  data: GroupListItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<GroupListItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: GroupListItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: GroupListItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'id': return compare(+a.id, +b.id, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
