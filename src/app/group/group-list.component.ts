import { AfterViewInit, Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { GroupListDataSource, GroupListItem } from './group-list-datasource';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements AfterViewInit, OnInit {
  pageTitle = 'Group List';
  imageWidth = 10;
  imageMargin = 2;
  showImage = false;

  // tslint:disable-next-line: variable-name
  listFilter: string;
  /*
  public get listFilter() {
    return this._listFilter;
  }
  public set listFilter(value) {
    this._listFilter = value;
    this.filteredDataSource = this.listFilter ? this.applyFilter(this.listFilter) : this.dataSource;
  }
  */

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<GroupListItem>;
  dataSource: GroupListDataSource;
  // filteredDataSource: GroupListDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['imageUrl', 'id', 'name', 'rating'];

  ngOnInit() {
    console.log('ngOnInit');
    // Replace with injectable !
    this.dataSource = new GroupListDataSource();
    // this.listFilter = 'gen';
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }

  toggleImage(): void {
    console.log('toggleImage');
    this.showImage = !this.showImage;
  }

  /*
  applyFilter(filterBy: string): GroupListDataSource {
    console.log('performFilter '+ this.listFilter);
    this.filteredDataSource = this.dataSource.data.filter((groupListItem: GroupListItem) =>
    groupListItem.name.toLocaleLowerCase().indexOf(filterBy.trim().toLocaleLowerCase()) !== -1);
    this.filteredDataSource.sort = this.sort;
    this.filteredDataSource.paginator = this.paginator;
    this.table.dataSource = this.filteredDataSource;
    return this.filteredDataSource;
  }
  */

  constructor(private changeDetectorRefs: ChangeDetectorRef) {
    console.log('constructur GroupListComponent');
  }

  onRatingClicked(message: string): void {
    console.log('In OnRatingClicked event received !');
  }
}
