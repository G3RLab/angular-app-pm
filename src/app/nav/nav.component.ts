import { Component, OnInit } from '@angular/core';

// use elements and classes from bootstrap styling framework

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  title = 'My first Angular Application People Management';

  constructor() { }

  ngOnInit() {
  }

  getTitle(): string {
    return this.title;
  }
}
