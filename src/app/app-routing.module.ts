import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './home/welcome.component';
import { GroupListComponent } from './group/group-list.component';
import { GroupDetailComponent } from './group/group-detail.component';
import { GroupDetailGuardGuard } from './group/group-detail-guard.guard';
import { UserSettingFormComponent } from './form/user-setting-form.component';
import { PmGroepDetailComponent } from './groep/pm-groep-detail.component';

/*  no leading slashes
    order matters specifiek after generic
*/
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'welcome' },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'groep', component: PmGroepDetailComponent },
  { path: 'groups', component: GroupListComponent },
  { path: 'groups/:id', canActivate: [GroupDetailGuardGuard], component: GroupDetailComponent },
  { path: 'forms', component: UserSettingFormComponent },
  // { path: '**', component: PageNotFoundComponent }
  { path: '**', redirectTo: 'welcome' }
];

@NgModule({
  /*  { useHash: true} to use hashstyle routes
      instead of html5 style routes
  */
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
