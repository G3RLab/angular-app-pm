import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PmGroep } from '../models/pm-groep';
import { PmGroepsleden } from '../models/pm-groepsleden';

@Injectable({
  providedIn: 'root'
})
export class PmDataServiceService {

  constructor(private http: HttpClient) { }

  getGroepByNaam(naam: string): Observable<PmGroep> {
    console.log('Start pm-data-service getGroepByNaam...')
    return this.http.get<PmGroep>('http://localhost:8081/groepen/v1/Honda');
  }
}


