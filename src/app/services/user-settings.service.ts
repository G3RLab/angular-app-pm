import { Injectable } from '@angular/core';
import { UserSettings } from '../models/user-settings';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserSettingsService {

  constructor(private http: HttpClient) { }

  getSubscriptionTypes(): Observable<string[]> {
    return of(['Monthly', 'Annual', 'Lifetime']);
  }

  postUsersettingsForm(userSettings: UserSettings) : Observable<any> {
    // rxjs function of to return Observable from input
    // return of(userSettings);

    // http works also with Observables so retrun is possible
    // return type generic changed from UserSettings to any so
    // can handle what ever post returns
    //
    // Real to PutsReq
    return this.http.post('https://putsreq.com/w499QdyAJI2kWbFSWrAG', userSettings);

  }
}
