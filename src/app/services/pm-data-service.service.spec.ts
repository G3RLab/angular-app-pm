import { TestBed } from '@angular/core/testing';

import { PmDataServiceService } from './pm-data-service.service';

describe('PmDataServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PmDataServiceService = TestBed.get(PmDataServiceService);
    expect(service).toBeTruthy();
  });
});
