import { Component, OnChanges, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-star',
  templateUrl: './star.component.html',
  styleUrls: ['./star.component.css']
})
export class StarComponent implements OnChanges {

  @Input() rating: number;
  startWidth: number;

  /* Output through raising an event
     TypeScript and generics
     Pass string or number or multiple values: define object
  */
   @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();

   onClick() {
    /* set to container */
    console.log(`In onClick ${this.rating} was clicked! `);
    this.ratingClicked.emit(`The rating ${this.rating} was clicked!`);
  }

  constructor() { }

  ngOnChanges() {
    // 75 related to html width
    this.startWidth = this.rating * 75 / 5;
  }

}
