import { Component, OnInit } from '@angular/core';
import { PmDataServiceService } from '../services/pm-data-service.service';
import { PmGroep } from '../models/pm-groep';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-pm-groep-detail',
  templateUrl: './pm-groep-detail.component.html',
  styleUrls: ['./pm-groep-detail.component.css']
})
export class PmGroepDetailComponent implements OnInit {

  groep: PmGroep;

  constructor(private pmDataService: PmDataServiceService) { }

  ngOnInit() {
    console.log('PmGroepDetailComponent ngOnInit start pm-data-service getGroepByNaam...');
    return this.pmDataService.getGroepByNaam('Honda').subscribe(
      (data: PmGroep) => this.groep = data,
      (err: any) => console.log(err),
      () => console.log('ngOnInit pm-data-service getGroepByNaam gereed')
    );
  }

}
