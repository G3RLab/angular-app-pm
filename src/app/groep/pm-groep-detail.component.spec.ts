import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PmGroepDetailComponent } from './pm-groep-detail.component';

describe('PmGroepDetailComponent', () => {
  let component: PmGroepDetailComponent;
  let fixture: ComponentFixture<PmGroepDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmGroepDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmGroepDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
