import { Component, OnInit } from '@angular/core';
import { UserSettings } from '../models/user-settings';
import { NgForm, NgModel } from '@angular/forms';
import { UserSettingsService } from '../services/user-settings.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-setting-form',
  templateUrl: './user-setting-form.component.html',
  styleUrls: ['./user-setting-form.component.css']
})
export class UserSettingFormComponent implements OnInit {

  originalUserSettings: UserSettings = {
    name: 'Post',
    emailOffers: true,
    intefaceStyle: 'dark',
    subscriptionType: 'Annual',
    notes: 'here are some notes...'
  };

  //  Protecting data if user clicks for example back or cancel
  // spread operator (...) copy each property
  // deep copy withdeep clone (for example see https://www.leonelngande.com/deep-cloning-objects-in-angular-typescript-javascript/)

  userSettings: UserSettings = { ...this.originalUserSettings};
  postError = false;
  postErrorMessage = '';
 //  subscriptionTypes = ['one', 'two', 'three'];
  subscriptionTypes: Observable<string[]>;

  // ngx-bootstrap toggle button
  singleModel = 'On';
  startDate: Date;
  startTime: Date;
  userRating = 0;
  maxRating = 10;
  isReadonly = false;

  constructor(private dataService: UserSettingsService ) { }

  ngOnInit() {
    this.subscriptionTypes = this.dataService.getSubscriptionTypes();
    this.startDate = new Date();
    this.startTime = new Date();
  }

  onSubmit(form: NgForm) {
    console.log('in onSubmit form.valid?: ', form.valid)
    // returns Observable
    // subscribe to Observable (async) to handle result with function to handle succes and error
    if (form.valid) {
    this.dataService.postUsersettingsForm(this.userSettings).subscribe(
      result => console.log('success: ', result),
    //  error =>  console.log('error: ', error),
      error =>  this.onHttpError(error)
    );
    } else {
      this.postError = true;
      this.postErrorMessage = 'Please fix the above errors';
    }
  }

  onHttpError(errorResponse: any) {
    console.log('in onHttpError errorResponse: ', errorResponse);
    this.postError = true;
    this.postErrorMessage = errorResponse.error.errorMessage;

  }

  onBlur(field: NgModel) {
    console.log('in oBlur field.valid?: ', field.valid)
  }
}
