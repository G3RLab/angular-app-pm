export interface IGroupListItem {
  id: number;
  name: string;
  imageUrl: string;
  rating: number;
}
