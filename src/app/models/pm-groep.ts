import {PmAbstractClass} from './pm-abstract-class';
import { PmGroepsleden } from './pm-groepsleden';

export class PmGroep extends PmAbstractClass {
    naam: string;
    waardeing: number;
    afbeeldingUrl: string;
    groepsleden: PmGroepsleden[];
}
