export class GroupListItem {

constructor(
    public id: number,
    public name: string,
    public imageUrl: string,
    public rating: number
    ) {
    }
}
