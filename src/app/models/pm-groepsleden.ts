import { PmAbstractClass } from './pm-abstract-class';

export class PmGroepsleden extends PmAbstractClass {
    groepUuid: string;
    persoonUuid: string;
}